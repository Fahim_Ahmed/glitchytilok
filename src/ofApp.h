#pragma once

#include "ofMain.h"
#include "ofxThreadedImage/src/ofxThreadedImage.h"
#include <ofxPanel.h>

class ofApp : public ofBaseApp{
private:
	ofxPanel gui;
	ofParameter<float> GLITCH_CI;

	ofstream output;
	ofFile lyricsFile;
	ofImage img;
	ofFile imgFile;
	
	string hexData;	
	ofTrueTypeFont font;

	ofBuffer *buffer;
	ofBuffer *lyricsBuffer;

	string IMAGE_NAME = "img.jpg";

	float iw, ih;
	int sw, sh;
	bool bProcessing;
	bool bDrawHex = false;

	public:
		void setup();
	void writeToBinary(ofBuffer& buffer, ofBuffer& lyricsBuffer);
	void saveImage();
	void hexOut(ofBuffer& buffer);
	void writeToHex(ofBuffer& buffer, ofBuffer& lyricsBuffer);
	void update();
		void draw();

		void keyPressed(int key);
	void redoProcess();
	void processOpenedFile(ofFileDialogResult result);
	void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
};
