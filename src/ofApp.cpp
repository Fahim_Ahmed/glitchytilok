#include "ofApp.h"

//#define GLITCH_CI 0.0001f

//--------------------------------------------------------------
void ofApp::setup(){
	ofBackground(24, 28, 33);

	sw = ofGetScreenWidth();
	sh = ofGetScreenHeight();

	GLITCH_CI = 0.0001f;
	GLITCH_CI.setMin(0.0001f);
	GLITCH_CI.setMax(0.001f);

	ofTrueTypeFont::setGlobalDpi(72);
	font.loadFont("consola.ttf", 14);

	imgFile.open(IMAGE_NAME, ofFile::ReadWrite, true);
	buffer = new ofBuffer(imgFile);

	lyricsFile.open("lyrics.txt", ofFile::ReadWrite, false);
	lyricsBuffer = new ofBuffer(lyricsFile);

	//BINARY WRITE
	writeToBinary(*buffer, *lyricsBuffer);
	img.load(*buffer);

	hexOut(*lyricsBuffer);

	//image size
	iw = img.getWidth();
	ih = img.getHeight();
	float ratio = iw / ih;

	ih = 720;
	iw = ih * ratio;



	gui.setup();
	gui.add(GLITCH_CI.set("Glitch CI", 0.0001f, 0.0001f, 0.001f));
}

void ofApp::writeToBinary(ofBuffer& buffer, ofBuffer& lyricsBuffer){
	int len = buffer.size();
	int count = 0;

	output.open("data/output.jpg", ios::out | ios::binary);

	//BINARY WRITE
	for (int i = 0; i < len; i++)
	{
		int val = unsigned(buffer.getData()[i]);
		if (val < 0) val += 256;

		if (ofRandom(1) < GLITCH_CI && (i + 1) % 16 == 1 && i != 1 && i+160 < len - 32)
		{
			// << ++count << endl;

			for (int j = 0; j < 160; j++)
			{
				buffer.getData()[i + j] = lyricsBuffer.getData()[j];
				output << buffer.getData()[i + j];
			}

			i += 159;
			continue;
		}
		output << buffer.getData()[i];
	}

	output << dec;
	output.close();
}

void ofApp::saveImage(){
	ofxThreadedImage imageSaver;
	imageSaver.allocate(sh, sh, OF_IMAGE_COLOR_ALPHA);

	glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);
	glReadPixels(0, 0, iw, ih, GL_RGBA, GL_UNSIGNED_BYTE, imageSaver.getPixels());

	string name = "GlitchyTilok/GT" + ofToString(ofGetUnixTime()) + ".jpg";
	imageSaver.saveImage(name, OF_IMAGE_QUALITY_BEST);
}

void ofApp::hexOut(ofBuffer& buffer){
	stringstream strStream;

	int len = buffer.size();
	for (int i = 0; i < len; i++)
	{
		int val = unsigned(buffer.getData()[i]);
		if (val < 0) val += 256;

		if (val < 16) strStream << hex << "0" << val;
		else strStream << hex << val;

		if (((i + 1) % 2 == 0) && i != 0) strStream << " ";
		if (((i + 1) % 16 == 0) && i != 0) strStream << "\n";
	}

	hexData = strStream.str();
}

void ofApp::writeToHex(ofBuffer& buffer, ofBuffer& lyricsBuffer){
	int len = buffer.size();
	output.open("data/output.hex", ios::out | ios::binary);

	for (int i = 0; i < len; i++)
	{
		int val = unsigned(buffer.getData()[i]);
		if (val < 0) val += 256;

		if (ofRandom(1) < GLITCH_CI && (i + 1) % 16 == 1 && i != 1)
		{
			for (int j = 0; j < 160; j++)
			{
				val = unsigned(lyricsBuffer.getData()[j]);
				if (val < 0) val += 256;

				if (val < 16) output << hex << "0" << val;
				else output << hex << val;
				if (((i + j + 1) % 16 == 0) && i != 0) output << "\n";
			}

			i += 159;
			continue;
		}

		if (val < 16) output << hex << "0" << val;
		else output << hex << val;
		if (((i + 1) % 16 == 0) && i != 0) output << "\n";
	}

	output << dec;
	output.close();
}

//--------------------------------------------------------------
void ofApp::update(){}

//--------------------------------------------------------------
void ofApp::draw(){
	ofPushMatrix();

	if (!bProcessing && img.isAllocated()) img.draw(0, 0, iw, ih);

	if (bDrawHex) font.drawString(hexData, 8, 16);

	gui.draw();

	ofPopMatrix();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	ofFileDialogResult openFileResult;

	if (key == ' ')
	{
		redoProcess();
	}
	else if (key == 'h' || key == 'H')
	{
		bDrawHex = !bDrawHex;
	}
	else if (key == 'o' || key == 'O')
	{
		openFileResult = ofSystemLoadDialog("Select a jpg");
		if (openFileResult.bSuccess)
		{
			processOpenedFile(openFileResult);
		}
		else
		{
			//do nothing
		}
	}
}

void ofApp::redoProcess(){
	bProcessing = true;
	imgFile.open(IMAGE_NAME, ofFile::ReadWrite, true);
	buffer = new ofBuffer(imgFile);
	writeToBinary(*buffer, *lyricsBuffer);
	img.load(*buffer);

	iw = img.getWidth();
	ih = img.getHeight();
	float ratio = iw / ih;

	ih = 720;
	iw = ih * ratio;

	bProcessing = false;
}

void ofApp::processOpenedFile(ofFileDialogResult result){
	ofFile file(result.getPath());

	if (file.exists())
	{
		string fileExtension = ofToUpper(file.getExtension());

		if (fileExtension == "JPG")
		{
			IMAGE_NAME = file.getAbsolutePath();
			redoProcess();
		}
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	sw = w;
	sh = h;
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){}
